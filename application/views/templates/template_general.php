<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title><? echo $title; ?></title>
        <?= HTML::style("assets/css/bootstrap.min.css"); ?>
        <?= HTML::style("assets/css/screen.css"); ?>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <?= HTML::script("assets/js/bootstrap.min.js"); ?>
        <?= HTML::script("assets/js/scripts.js"); ?>
    </head>
    <body>
        <div id="wrapper">
            <!-- Header for the site -->
            <div id="header">
            		<a id="logo" href="/admin"><img src="/assets/images/logo.png" alt="Service Master"></a>
		            <!-- Menu for the site -->
		            <div id="navlist">
		                <ul class="nav">
		                    <li><?php echo HTML::anchor("general", "Dashboard"); ?></li>
		                    <li><?php echo HTML::anchor("common/logout", "Logout"); ?></li>
		                </ul>
		            </div>
            </div>
            <!-- here we load all content -->
            <div id="content" class="container">
                <?php echo $content; ?>
            </div>
             
            <!-- Footer for the site -->
            <div id="footer">
                <p id="copyright">&copy; <?= date('Y') ?> All Rights Reserved.</p>
            </div>
        </div>
    </body>
</html>