<div id="headerBox">
	<h1>Sign Up</h1>
</div>
<? if ($message) : ?>
            <h3 class="message">
                        <?= $message; ?>
            </h3>
<? endif; ?>
<?php if (!empty($errors)): ?>
    <div class="alert alert-danger" role="alert">
        <p class="message">Some errors were encountered, please check the details you entered.</p>
        <ul class="errors">
            <?php foreach ($errors as $message): ?>
                <li><?php print_r($message) ?></li>
            <?php endforeach ?>
        </ul>
    </div>
<?php endif ?>

<?= Form::open('public/register'); ?>
<div class="form-group">
<?= Form::label('username', 'Username'); ?>
<?= Form::input('username', HTML::chars(Arr::get($_POST, 'username')), array('id' => 'username', 'class' => 'form-control')); ?>
</div>

<div class="form-group">
<?= Form::label('email', 'Email Address'); ?>
<?= Form::input('email', HTML::chars(Arr::get($_POST, 'email')), array('id' => 'email', 'class' => 'form-control')); ?>

</div>

<div class="form-group">
<?= Form::label('password', 'Password'); ?>
<?= Form::password('password', null, array('id' => 'password', 'class' => 'form-control')); ?>

</div>

<div class="form-group">
<?= Form::label('password_confirm', 'Confirm Password'); ?>
<?= Form::password('password_confirm', null, array('id' => 'password_confirm', 'class' => 'form-control')); ?>

</div>

<div class="form-group">
<?= Form::select('role', $roles, HTML::chars(Arr::get($_POST, 'role')), array('class' => 'form-control')); ?>
</div>
<?= Form::submit('register', 'Register', array('class' => 'btn btn-submit')); ?>
<?= Form::close(); ?>