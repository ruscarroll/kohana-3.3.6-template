
	<div id="headerBox">
		<h1>Login</h1>
	</div>
	<? if ($message) : ?>
	  <h2 class="message"><?= $message; ?></h2>
	<? endif; ?>
	<?= Form::open('public/login', array('role' => 'form')); ?>
	<div class="form-group">
		<?= Form::label('username', 'User name'); ?>
		<?= Form::input('username', HTML::chars(Arr::get($_POST, 'username')), array('id' => 'username', 'class' => 'form-control')); ?>
	</div>
	<div class="form-group">
		<?= Form::label('password', 'Password'); ?>
		<?= Form::password('password', null, array('id' => 'password', 'class' => 'form-control')); ?>
	</div>
	<?= Form::submit('login', 'Login', array('class' => 'btn btn-submit')); ?>
	<?= Form::close(); ?>
