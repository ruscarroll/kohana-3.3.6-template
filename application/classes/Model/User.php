<?php defined('SYSPATH') or die('No direct script access.');

class Model_User extends Model_Auth_User {

    public function getRoles()
    {
        // get all the roles available to a user
        $roles = DB::select()
            ->from('roles')
            ->execute();
            
        return $roles->as_array();
    }

    public function getSelectRoles()
    {
        // this formats the roles so Form::select can process them in a select menu
        $new_roles_array = null;
        if ($roles = $this->getRoles())
        {
            // we want to ignore the login role because everyone gets it
            unset($roles[0]);
            foreach($roles as $role)
            {
                $new_roles_array[$role['name']] = $role['name'];
            }
        }
        return $new_roles_array;
    }

    public function getBestRoleForUser($user_id)
    {
        //everybody has the login role so get the role that isn't login
        // get all the roles available to a user
        $roles = DB::query(Database::SELECT, "SELECT name FROM roles, roles_users WHERE roles.id = roles_users.role_id AND roles_users.user_id = {$user_id} AND roles.name != 'login' LIMIT 0, 1")
            ->execute();
        $role = current($roles->as_array());
        if (!empty($role))
        {
            $role = $role['name'];
        } else
        {
            $role = false;
        }
        return $role;
    }

    public function redirectBasedOnRole($role)
    {
        // Load the user information
        $user = Auth::instance()->get_user();
        // if logged in
        if ($user)
        {
            $best_role = $this->getBestRoleForUser($user->id);
            if ($role !== $best_role)
            {
                // if someone is trying to access without permission then redirect them to the proper place
               HTTP::redirect($best_role);
            }
        }
        else
        {
            // user is not logged it to send them to login
           HTTP::redirect('public/login');
        }

    }


}
