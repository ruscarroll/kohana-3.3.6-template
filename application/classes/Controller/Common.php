<?php defined('SYSPATH') or die('No direct script access.');
// Controller for all the public pages
class Controller_Common extends Controller {
    // these are all common controller items that don't require any permissions and should not render directly because it doesn't extend templates currently
    public function action_logout()
    {
        // Log user out
        Auth::instance()->logout();

        // Redirect to login page
        $this->redirect('public/login');
    }
}