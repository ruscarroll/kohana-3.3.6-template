<?php defined('SYSPATH') or die('No direct script access.');
// Controller for all the public pages
class Controller_Public extends Controller_DefaultTemplate {

    public $template = 'templates/template_public';

    public function before()
    {
        parent::before();
        $user = Model::factory('User');
        $auth_user = Auth::instance()->get_user();
        // if we have a user then we are logged in and lets send them to the right page
        if ($auth_user)
        {
            $role = $user->getBestRoleForUser($auth_user->id);
            $this->redirect($role);
        }
    }

    public function action_index()
    {
        $this->template->title = "Home";
        $data = "Home page content!!!";
        $this->template->content = View::factory('public/home')
            ->bind('data', $data);
    }

    public function action_login()
    {
        $user = Model::factory('User');
        $this->template->title = "Login";
        $this->template->content = View::factory('public/login')
            ->bind('message', $message);

        if (HTTP_Request::POST == $this->request->method())
        {
            // Attempt to login user
            $remember = array_key_exists('remember', $this->request->post()) ? (bool) $this->request->post('remember') : FALSE;
            $auth_user = Auth::instance()->login($this->request->post('username'), $this->request->post('password'), $remember);
            // If successful, redirect user
            if ($auth_user)
            {
                // lets really get the user
                $auth_user = Auth::instance()->get_user();
                $role = $user->getBestRoleForUser($auth_user->id);
                $this->redirect($role);
            }
            else
            {
                $message = 'Login failed';
            }
        }
    }

    public function action_register()
    {
        $this->template->title = "Sign Up";
        $this->template->content = View::factory('user/create')
            ->bind('roles', $roles)
            ->bind('errors', $errors)
            ->bind('message', $message);

        $user = Model::factory('User');
        $roles = $user->getSelectRoles();

        if (HTTP_Request::POST == $this->request->method())
        {
            try {
                // Create the user using form values
                $user = ORM::factory('User')->create_user($this->request->post(), array(
                    'username',
                    'password',
                    'email'
                ));


                // Grant user the role
                $user->add('roles', ORM::factory('Role', array('name' => $this->request->post('role'))));

                // everybody gets the login role so they can login
                $user->add('roles', ORM::factory('Role', array('name' => 'login')));

                // Reset values so form is not sticky
                $_POST = array();

                // Set success message
                $message = "You have added user '{$user->username}' to the database";

            } catch (ORM_Validation_Exception $e) {

                // Set failure message
                $message = 'There were errors, please see form below.';

                // Set errors using custom messages
                $errors = $e->errors('models');
            }
        }
    }
}