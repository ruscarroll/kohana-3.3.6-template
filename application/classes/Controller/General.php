<?php defined('SYSPATH') or die('No direct script access.');
// Controller for all the public pages
class Controller_General extends Controller_DefaultTemplate {

    public $template = 'templates/template_general';

    public function before()
    {
        parent::before();
        $user = Model::factory('User');
        $user->redirectBasedOnRole('general');
    }

    public function action_index()
    {
        // this is the dashboard
        $this->template->title = "General Dashboard";
        $data = "page content!!!";
        $this->template->content = View::factory('general/dashboard')
            ->bind('data', $data);
    }

}