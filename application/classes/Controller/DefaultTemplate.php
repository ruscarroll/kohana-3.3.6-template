<?php defined('SYSPATH') or die('No direct script access.');
// Controller for all the public pages
class Controller_DefaultTemplate extends Controller_Template {

    public $template = 'templates/template_public';

    public function before()
    {
        parent::before();
        // this allows us to easily change the title of a page within the controller
        $this->template->title = 'Default Template Title';
    }

}