<?php defined('SYSPATH') or die('No direct script access.');
// Controller for all the public pages
class Controller_Admin extends Controller_DefaultTemplate {

    public $template = 'templates/template_admin';

    public function before()
    {
        parent::before();
        $user = Model::factory('User');
        $user->redirectBasedOnRole('admin');
    }

    public function action_index()
    {
        // this is the dashboard
        $this->template->title = "Admin Dashboard";
        $data = "page content!!!";
        $this->template->content = View::factory('admin/dashboard')
            ->bind('data', $data);
    }

}