$(document).ready(function() {
	
	$('.btn').tooltip();

	if ($('.btn-delete-confirm').length > 0) {
		$('.btn-delete-confirm').on('click', Videos.launchDeleteModal);
	}

	if ($('.btn-video-modal').length > 0) {
		$('.btn-video-modal').on('click', Videos.launchVideoModal);

		$('#viewVideoModal').on('hidden.bs.modal', Videos.pauseVideoPlayer);
	}

	Videos.init();

	if ($('#marketing-info').length > 0) {
		Customize.init();
	}
});

var Customize = (function() {
	var form = {};

	function checkMarketing(e) {

		form.errors = [];
		$('.marketing-error').hide();

		if ($('#marketing_license').val() == '') {
				form.errors.push('license');
		}

		if ($('#marketing_check').is(':checked')) {
			
			if ($('#marketing_email').val() == '') {
					form.errors.push('email');
			}
		}

		if (form.errors.length > 0) {
			e.preventDefault();
			renderErrors();
		}
	}

	function renderErrors() {
		var max = form.errors.length;
		for (var i = 0; i < max; i++) {
			$('#marketing_error_' + form.errors[i]).fadeIn();
		}
	}

	function toggleMarketingInfo() {
		$('#marketing-info').toggle();
	}

	form.errors = [];

	form.init = function() {
		form.attachHandlers();
	};

	form.attachHandlers = function() {
		$('#download-btn').on('click', checkMarketing);
		$('#marketing_check').on('change', toggleMarketingInfo);
	};

	return form;

}());

var Videos = (function() {

	return {
		videoplayer: null,
		
		init: function() {
			this.videoplayer = $('#videoplayer')[0];
		},

		launchDeleteModal: function(e) {
			var video_id = $(this).data('id');
			$('#js-delete').attr('href', 'deleteVideo/' + video_id );
			$('#deleteVideoModal').modal({
				show: true
			});
		},

		launchVideoModal: function(e) {
			e.preventDefault();
			var video_src = $(this).attr('href');
			Videos.videoplayer.src = video_src;
			$('#viewVideoModal').modal({
				show: true
			});
		},

		removeVideoPlayer: function() {
			Videos.videoplayer.html('');
		},

		pauseVideoPlayer: function() {
			Videos.videoplayer.pause();
		}
	}

}());